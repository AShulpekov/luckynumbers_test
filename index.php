<?php
require_once "LuckyNumber.php";

try {
    $luckyNumber = new LuckyNumber($_GET['first'], $_GET['end']);
    $luckyNumber->calculateLuckyNumbers();
    var_export(count($luckyNumber->getLuckyNumbers()));
} catch (Exception $exception) {
    echo $exception->getMessage();
} catch (Error $fatal) {
    var_export('<br>FATAL ERROR EX: <br>' . $fatal->getMessage());
}