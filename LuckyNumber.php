<?php
/**
 * Created by PhpStorm.
 * User: ash
 * Date: 29.06.19
 * Time: 17:28
 */


class LuckyNumber
{
    private $firstTicketNum = 0;
    private $lastTicketNum = 0;
    private $luckyNumbers = [];


    /**
     * LuckyNumber constructor.
     * @param int $firstTicketNum
     * @param int $lastTicketNum
     * @throws \Exception - validation input data exceptions
     */
    public function __construct(int $firstTicketNum, int $lastTicketNum)
    {
        if (!isset($firstTicketNum) && !isset($lastTicketNum)) {
            throw new \Exception("Error during parse input data. FirstTicketNum: " .
                var_export($firstTicketNum, true) . "; LastTicketException: " .
                var_export($lastTicketNum, true));
        }

        if ($firstTicketNum > $lastTicketNum) {
            throw new \Exception("Error, first num shouldnt be much that last!!!" .
                var_export($firstTicketNum, true) . "; LastTicketException: " .
                var_export($lastTicketNum, true));
        }

        $this->firstTicketNum = $firstTicketNum;
        $this->lastTicketNum = $lastTicketNum;
    }

    public function getLuckyNumbers(): array
    {
        return $this->luckyNumbers;
    }

    private function calculateSumOfNumbers(string $stringNum): int
    {
        $sum = array_sum(str_split($stringNum));
        if ($sum > 9) {
            return $this->calculateSumOfNumbers($sum);
        }

        return $sum;
    }

    /**
     * @param
     */
    public function calculateLuckyNumbers(): void
    {
        for ($i = $this->firstTicketNum; $i <= $this->lastTicketNum; $i++) {
            $ticketStr = str_pad($i, 6, '0', STR_PAD_LEFT);
            $currentTicketArr = $this->getSplittedTicketNum($ticketStr);
            if ($this->calculateSumOfNumbers($currentTicketArr['firstPart']) ===
                $this->calculateSumOfNumbers($currentTicketArr['secondPart'])) {
                $this->luckyNumbers[] = $ticketStr;
            }
        }
    }

    private function getSplittedTicketNum(string $ticketNum)
    {
        return [
            'firstPart' => substr($ticketNum, 0, 3),
            'secondPart' => substr($ticketNum, 3, 6)
        ];
    }
}